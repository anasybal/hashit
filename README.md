# HashIt
hashit is a simple Qt Based Text hashing tool, it uses the QCryptographicHash library for its backend

# Screenshots
![1](https://i.imgur.com/7iyHoCR.png "Screenshot 1")
![1](https://i.imgur.com/W2TZeep.png "Screenshot 2")
![1](https://i.imgur.com/bAqb7Y7.png "Screenshot 3")

## Support
You can help me to stay online by donating some moneros at: `497vShTje6gDvQqM86M5PoYLMsPdsqvgxHtRYAGMkamXhbNB7ymVvxtBHCcjutrCVx2PByc4TJ75XWi3zetpehi4Eerrmo3`.
or also Libyana to my phone: `+218924627244`
