#!/usr/bin/env bash


if [ $1 ]; then
    PROJECT_NAME=$1 # setting project name from the first argument
else
    PROJECT_NAME="hashit" # The default project name
fi

for i in 16 20 22 24 32 36 44 48 64 72 96 128 150 160 192 256 310 384 512 800
do
   inkscape hashit.svg -o "${i}-apps-${PROJECT_NAME}.png" -w $i -h $i
done
