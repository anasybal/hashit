/**
 ** This file is part of the Hashit project.
 ** Copyright 2022 Anas Yousef <anasybal@mail.ru>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#pragma once

#include <QMainWindow>
#include <QCryptographicHash>
#include "about.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class HashIt; }
QT_END_NAMESPACE

class HashIt : public QMainWindow
{
    Q_OBJECT
public:
    HashIt(QWidget *parent = nullptr);
    ~HashIt();

private slots:
    void hash_text();
    void show_info();
    void hash_text_changed();
    void hash_algorithm_changed(int algorithm_index);

private:
    Ui::HashIt *ui;
    QByteArray make_hash(QByteArray plain_text, QCryptographicHash::Algorithm algorithm);
    QCryptographicHash::Algorithm define_algorithm(int algorithm_index);
};
