/**
 ** This file is part of the Hashit project.
 ** Copyright 2022 Anas Yousef <anasybal@mail.ru>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "include/hashit.hpp"
#include "../data/ui/ui_hashit.h"

HashIt::HashIt(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::HashIt)
{
    // Setting up the user-interface from `../data/ui/hashit.ui` :
    ui->setupUi(this);

    // Setting Icon :
    setWindowIcon(QIcon::fromTheme("hashit"));

    // Objects connections :
    connect(ui->btn_hash, &QPushButton::clicked, this, &HashIt::hash_text);
    connect(ui->btn_info, &QPushButton::clicked, this, &HashIt::show_info);
    connect(ui->plain_text_input, &QPlainTextEdit::textChanged, this, &HashIt::hash_text_changed);
    connect(ui->hash_algorithm, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &HashIt::hash_algorithm_changed);
    connect(ui->auto_hash, &QPushButton::clicked, this, [=](bool checked){
        if(checked){
            ui->auto_hash->setIcon(QIcon(":/auto_hash_on.svg"));
        }else{
            ui->auto_hash->setIcon(QIcon(":/auto_hash_off.svg"));
        }
    });
}

HashIt::~HashIt()
{
    delete ui;
}

// Private Slots Functions :

void HashIt::hash_text()
{
    if(ui->plain_text_input->toPlainText() != nullptr && ui->plain_text_input->toPlainText() != ""){
        QByteArray hash;
        QCryptographicHash::Algorithm algo = define_algorithm(ui->hash_algorithm->currentIndex());
        hash = make_hash(ui->plain_text_input->toPlainText().toUtf8(), algo);
        ui->hash_output->setText(hash);
    } else ui->hash_output->clear();
}

void HashIt::show_info()
{
    About abt;
    abt.exec();
}

void HashIt::hash_text_changed()
{
    if(ui->auto_hash->isChecked()){
        if(ui->plain_text_input->toPlainText() != nullptr && ui->plain_text_input->toPlainText() != ""){
            QByteArray hash;
            QCryptographicHash::Algorithm algo = define_algorithm(ui->hash_algorithm->currentIndex());
            hash = make_hash(ui->plain_text_input->toPlainText().toUtf8(), algo);
            ui->hash_output->setText(hash);
        } else ui->hash_output->clear();
    } else return;
}

void HashIt::hash_algorithm_changed(int algorithm_index)
{
    if(ui->plain_text_input->toPlainText() != nullptr && ui->plain_text_input->toPlainText() != ""){
        QByteArray hash;
        QCryptographicHash::Algorithm algo = define_algorithm(algorithm_index);
        hash = make_hash(ui->plain_text_input->toPlainText().toUtf8(), algo);
        ui->hash_output->setText(hash);
    }
}

// Private Functions :

QByteArray HashIt::make_hash(QByteArray plain_text, QCryptographicHash::Algorithm algorithm)
{
    return QCryptographicHash::hash(plain_text, algorithm).toHex();
    /// define_algorithm(ui->hash_algorithm->currentIndex());
}

QCryptographicHash::Algorithm HashIt::define_algorithm(int algorithm_index)
{
    QCryptographicHash::Algorithm algo;
    switch(algorithm_index){
        case 0:
            algo = QCryptographicHash::Algorithm::Md5;
            break;
        case 1:
            algo = QCryptographicHash::Algorithm::Md4;
            break;
        case 2:
            algo = QCryptographicHash::Algorithm::Sha1;
            break;
        case 3:
            algo = QCryptographicHash::Algorithm::Sha224;
            break;
        case 4:
            algo = QCryptographicHash::Algorithm::Sha256;
            break;
        case 5:
            algo = QCryptographicHash::Algorithm::Sha384;
            break;
        case 6:
            algo = QCryptographicHash::Algorithm::Sha512;
            break;
        case 7:
            algo = QCryptographicHash::Algorithm::Keccak_224;
            break;
        case 8:
            algo = QCryptographicHash::Algorithm::Keccak_256;
            break;
        case 9:
            algo = QCryptographicHash::Algorithm::Keccak_384;
            break;
        case 10:
            algo = QCryptographicHash::Algorithm::Keccak_512;
            break;
        case 11:
            algo = QCryptographicHash::Algorithm::RealSha3_224;
            break;
        case 12:
            algo = QCryptographicHash::Algorithm::RealSha3_224;
            break;
        case 13:
            algo = QCryptographicHash::Algorithm::RealSha3_256;
            break;
        case 14:
            algo = QCryptographicHash::Algorithm::RealSha3_384;
            break;
        case 15:
            algo = QCryptographicHash::Algorithm::RealSha3_512;
            break;
        case 16:
            algo = QCryptographicHash::Algorithm::Sha3_224;
            break;
        case 17:
            algo = QCryptographicHash::Algorithm::Sha3_256;
            break;
        case 18:
            algo = QCryptographicHash::Algorithm::Sha3_384;
            break;
        case 19:
            algo = QCryptographicHash::Algorithm::Sha3_512;
            break;
        default:
            algo = QCryptographicHash::Algorithm::Md5;
    }
    return algo;
}
